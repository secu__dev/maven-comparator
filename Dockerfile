FROM alpine/git
WORKDIR /app
RUN git clone https://github.com/siom79/japicmp.git

FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY --from=0 /app/japicmp /app
RUN mvn clean package

FROM openjdk:8-jre-alpine

WORKDIR /app
COPY --from=1 /app/japicmp/target/japicmp-*-jar-with-dependencies.jar /app/japicmp.jar
COPY zhu_li_do_the_thing.sh /app
RUN apk update && apk add --no-cache curl ca-certificates

RUN chmod a+x /app/zhu_li_do_the_thing.sh
ENTRYPOINT ["ash", "/app/zhu_li_do_the_thing.sh"]