# $1 = groupId
# $2 = artifactId
# $3 = old version
# $4 = new version

if [ "$#" -ne 4 ]; then
  echo "Usage: GroupId ArtifactId OldVersion NewVersion"
  exit 1
fi

groupPath=$1
groupPath=${groupPath//\.//}

curl https://repo1.maven.org/maven2/$groupPath/$2/$3/$2-$3.jar -o cp_source.jar > /dev/null
curl https://repo1.maven.org/maven2/$groupPath/$2/$4/$2-$4.jar -o cp_dest.jar > /dev/null

java $JAVA_OPTS -cp $(echo cp_*.jar | tr ' ' ':') -jar /app/japicmp.jar -o cp_source.jar -n cp_dest.jar --ignore-missing-classes --only-modified